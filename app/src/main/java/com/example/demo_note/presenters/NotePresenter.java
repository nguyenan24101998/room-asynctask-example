package com.example.demo_note.presenters;

import android.app.Application;
import android.content.Context;

import com.example.demo_note.models.NoteDetail;
import com.example.demo_note.rooms.managers.NoteManager;
import com.example.demo_note.views.MainView;

import java.util.List;

public class NotePresenter {
    private Application mApplication;
    private Context mContext;
    private MainView mMainView;
    private NoteManager mNoteManager;

    public NotePresenter(Application mApplication, Context mContext, MainView mMainView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mMainView = mMainView;
        this.mNoteManager = new NoteManager(mApplication);
    }

    public void Add(final NoteDetail noteDetail){
        mNoteManager.insertNote(noteDetail, new NoteManager.OnDataCallBack() {
            @Override
            public void onDataSuccess(List<NoteDetail> noteDetails) {
                mMainView.addSuccess(noteDetail);
            }

            @Override
            public void onDataFail() {
                mMainView.addFail("Add fail");
            }
        });
    }
}
