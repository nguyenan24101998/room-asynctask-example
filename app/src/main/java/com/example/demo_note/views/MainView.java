package com.example.demo_note.views;

import com.example.demo_note.models.NoteDetail;

public interface MainView {
    public void addSuccess(NoteDetail noteDetail);
    public void addFail(String message);
}
