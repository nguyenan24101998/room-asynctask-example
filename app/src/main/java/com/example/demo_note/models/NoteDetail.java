package com.example.demo_note.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
@Entity
public class NoteDetail {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo (name = "note_title")
    private String name;
    @ColumnInfo (name = "note_decrisption")
    private String noiDung;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }
}
