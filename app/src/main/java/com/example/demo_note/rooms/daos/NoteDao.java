package com.example.demo_note.rooms.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.demo_note.models.NoteDetail;

import java.util.List;

@Dao
public interface NoteDao {
    @Insert
    void insertNote(NoteDetail... notes);
    @Query("SELECT * FROM NoteDetail")
    List<NoteDetail> getAllNotes();
    /*@Query("DELETE FROM NoteDetail WHERE id = :noteId")
    void deleteNote(int noteId);

    @Query("UPDATE NoteDetail SET note_context = :context WHERE id = :id")
    void updateNote(String context,int id);

    @Query("SELECT * FROM Note WHERE noteId = :noteId ")
    NoteDetail getInfoNote(int noteId);*/
}
