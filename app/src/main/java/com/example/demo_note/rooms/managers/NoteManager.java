package com.example.demo_note.rooms.managers;

import android.content.Context;
import android.os.AsyncTask;

import com.example.demo_note.models.NoteDetail;
import com.example.demo_note.rooms.daos.NoteDao;
import com.example.demo_note.rooms.databases.NoteDatabase;

import java.util.List;

public class NoteManager  {
    private NoteDao mNoteDao;
    private Context mContext;

    public NoteManager(Context mContext) {
        NoteDatabase noteDatabase = NoteDatabase.getRiderInstance(mContext);
        this.mNoteDao = noteDatabase.noteDao();
        this.mContext = mContext;
    }

    public interface OnDataCallBack{
        void onDataSuccess(List<NoteDetail> noteDetails);
        void onDataFail();
    }

    public void insertNote(NoteDetail note, OnDataCallBack listener) {
        InsertNoteAsync insertNoteAsync = new InsertNoteAsync(mNoteDao,listener);
        insertNoteAsync.execute(note);
    }


    private class InsertNoteAsync extends AsyncTask<NoteDetail, Void, Void> {

        private NoteDao mNoteDao;
        private OnDataCallBack listener;

        public InsertNoteAsync(NoteDao mNoteDao, OnDataCallBack listener) {
            this.mNoteDao = mNoteDao;
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(NoteDetail... notes) {
            mNoteDao.insertNote(notes);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    /*public void showListNote(OnDataCallBack listener) {
        ShowListNoteSync showListNoteSync = new ShowListNoteSync(mNoteDao, listener);
        showListNoteSync.execute();
    }

    public void deleteNote(int id,OnDataCallBack listener){
        DeleteNoteAsync deleteNoteAsync = new DeleteNoteAsync(mNoteDao,listener);
        deleteNoteAsync.execute(id);
    }

    public void updateNote(Note note,OnDataCallBack listener){
        UpdateNoteAsync updateNoteAsync = new UpdateNoteAsync(mNoteDao,listener);
        updateNoteAsync.execute(note);
    }


    private class DeleteNoteAsync extends AsyncTask<Integer , Void, Void> {
        private NoteDao mNoteDao;
        private OnDataCallBack mOnDataCallBack;

        public DeleteNoteAsync(NoteDao mNoteDao, OnDataCallBack mOnDataCallBack) {
            this.mNoteDao = mNoteDao;
            this.mOnDataCallBack = mOnDataCallBack;
        }


        @Override
        protected Void doInBackground(final Integer... integers) {
            mNoteDao.deleteNote(integers[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mOnDataCallBack.onDataSuccess(null);
        }
    }

    private class ShowListNoteSync extends AsyncTask<List<Note>, Void, Void> {

        private NoteDao mNoteDao;
        private OnDataCallBack mOnDataCallBack;
        private List<Note> mNotes;

        public ShowListNoteSync(NoteDao mNoteDao, OnDataCallBack mOnDataCallBack) {
            this.mNoteDao = mNoteDao;
            this.mOnDataCallBack = mOnDataCallBack;
        }

        @Override
        protected Void doInBackground(List<Note>... lists) {
            mNotes = mNoteDao.getAllNotes();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mNotes != null) {
                mOnDataCallBack.onDataSuccess(mNotes);
            } else {
                mOnDataCallBack.onDataFail();
            }
        }
    }
    private class UpdateNoteAsync extends AsyncTask<Note,Void,Void>{

        private NoteDao mNoteDao;
        private OnDataCallBack mOnDataCallBack;

        public UpdateNoteAsync(NoteDao mNoteDao, OnDataCallBack mOnDataCallBack) {
            this.mNoteDao = mNoteDao;
            this.mOnDataCallBack = mOnDataCallBack;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mNoteDao.updateNote(notes[0].getNoteContext(),notes[0].getNoteId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mOnDataCallBack.onDataSuccess(null);
        }
    }*/
}
