package com.example.demo_note.rooms.databases;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.demo_note.models.NoteDetail;
import com.example.demo_note.rooms.daos.NoteDao;

import static com.example.demo_note.rooms.databases.NoteDatabase.DATABASE_VERSION;

@Database(entities = {NoteDetail.class}, exportSchema = false, version = DATABASE_VERSION)
public abstract class NoteDatabase extends RoomDatabase {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "note-database";
    private static NoteDatabase INSTANCE;

    public abstract NoteDao noteDao();

    public static synchronized NoteDatabase getRiderInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (NoteDao.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        NoteDatabase.class,
                        DATABASE_NAME).fallbackToDestructiveMigration().build();
            }
        }
        return INSTANCE;
    }
}