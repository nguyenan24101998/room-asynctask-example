package com.example.demo_note.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.demo_note.R;
import com.example.demo_note.models.NoteDetail;
import com.example.demo_note.presenters.NotePresenter;
import com.example.demo_note.views.MainView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainView {
    private RecyclerView mRecyclerView;
    private Button mBtnAdd;
    private NotePresenter mNotePresenter;
    private NoteDetail mNoteDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.rcvNote);
        mBtnAdd = findViewById(R.id.btn_Add);
        mBtnAdd.setOnClickListener(this);

        mNotePresenter =  new NotePresenter(getApplication(),this,this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_Add:
                mNotePresenter.Add(mNoteDetail);
                break;
        }
    }

    @Override
    public void addSuccess(NoteDetail noteDetail) {
        Toast.makeText(this,"Thanh Cong",Toast.LENGTH_LONG).show();
    }

    @Override
    public void addFail(String message) {
        Toast.makeText(this,"That Bai",Toast.LENGTH_LONG).show();
    }
}
