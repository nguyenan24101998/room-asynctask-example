package com.example.demo_note.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo_note.R;
import com.example.demo_note.models.NoteDetail;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    private Context context;
    private List<NoteDetail> noteDetails;

    public NoteAdapter(Context context, List<NoteDetail> noteDetails) {
        this.context = context;
        this.noteDetails = noteDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtName.setText(noteDetails.get(position).getName());
        holder.txtNoiDung.setText(noteDetails.get(position).getNoiDung());
    }

    @Override
    public int getItemCount() {
        return (noteDetails!=null)? noteDetails.size():0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtName, txtNoiDung;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtNoiDung = itemView.findViewById(R.id.txtNoiDung);
        }
    }
}